<?php

namespace Drupal\statistics_snapshots;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Statistics snapshot entities.
 *
 * @ingroup statistics_snapshots
 */
class StatisticsSnapshotListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Statistics snapshot ID');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\statistics_snapshots\Entity\StatisticsSnapshot $entity */
    $row['id'] = Link::createFromRoute(
      $entity->id(),
      'entity.statistics_snapshot.edit_form',
      ['statistics_snapshot' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }
}
