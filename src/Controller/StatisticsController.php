<?php

namespace Drupal\statistics_snapshots\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\statistics_snapshots\Entity\StatisticsSnapshot;

/**
 * Controller for statistics snapshots.
 *
 * @todo do we need this for any functionality?
 */
class StatisticsController extends ControllerBase implements ContainerInjectionInterface {
}
