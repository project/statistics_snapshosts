<?php

namespace Drupal\statistics_snapshots\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorInterface;

/**
 * Provides an interface for defining Statistics snapshot entities.
 *
 * @ingroup statistics_snapshots
 */
interface StatisticsSnapshotInterface extends ContentEntityInterface {

  /**
   * Gets the Statistics snapshot creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Statistics snapshot.
   */
  public function getCreatedTime();

  /**
   * Sets the Statistics snapshot creation timestamp.
   *
   * @param int $timestamp
   *   The Statistics snapshot creation timestamp.
   *
   * @return \Drupal\statistics_snapshots\Entity\StatisticsSnapshotInterface
   *   The called Statistics snapshot entity.
   */
  public function setCreatedTime($timestamp);


  /**
   * Set the value of a statistic on this snapshot.
   *
   * You need to use this function, rather than ->set(),
   * to allow the plugins to have useful fieldnames (for DX),
   * but not exceed 32 characters for field name length.
   *
   * @param \Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorInterface $plugin
   *   Plugin instance.
   * @param string $fieldName
   *   Statistic full fieldname.
   * @param float $value
   *   Value to set.
   */
  public function setStatValue(StatisticsSnapshotsCalculatorInterface $plugin, string $fieldName, float $value);

  /**
   * Increment the value of a statistic on this snapshot.
   *
   * You need to use this function, rather than ->set(),
   * to allow the plugins to have useful fieldnames (for DX),
   * but not exceed 32 characters for field name length.
   *
   * @param \Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorInterface $plugin
   *   Plugin instance.
   * @param string $fieldName
   *   Statistic full fieldname.
   * @param int $value
   *   Value to set.
   */
  public function incrementStatValue(StatisticsSnapshotsCalculatorInterface $plugin, string $fieldName, $value);

  /**
   * Convert a friendly plugin fieldname to a hash for use as drupal fieldname.
   *
   * @param \Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorInterface $plugin
   *   Plugin instance.
   * @param string $fieldName
   *   Field name.
   *
   * @return string
   *   Hash of fieldname (which is what is used as the fieldname for drupal)
   */
  public static function getFieldHash(StatisticsSnapshotsCalculatorInterface $plugin, string $fieldName);

  /**
   * Get the value of a statistic.
   *
   * @param \Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorInterface $plugin
   *   Plugin instance.
   * @param string $fieldName
   *   Field name.
   *
   * @return float|null
   *   Statistic value.
   */
  public function getStatValue(StatisticsSnapshotsCalculatorInterface $plugin, string $fieldName);
}
