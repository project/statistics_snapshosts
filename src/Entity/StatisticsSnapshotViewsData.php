<?php

namespace Drupal\statistics_snapshots\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Statistics snapshot entities.
 */
class StatisticsSnapshotViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }
}
