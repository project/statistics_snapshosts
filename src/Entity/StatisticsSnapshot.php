<?php

namespace Drupal\statistics_snapshots\Entity;

use DateInterval;
use DateTime;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorInterface;
use Exception;

class StatisticsGenerationException extends Exception {
}

/**
 * Defines the Statistics snapshot entity.
 *
 * @ingroup statistics_snapshots
 *
 * @ContentEntityType(
 *   id = "statistics_snapshot",
 *   label = @Translation("Statistics snapshot"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\statistics_snapshots\StatisticsSnapshotListBuilder",
 *     "views_data" = "Drupal\statistics_snapshots\Entity\StatisticsSnapshotViewsData",
 *
 *     "access" = "Drupal\statistics_snapshots\StatisticsSnapshotAccessControlHandler",
 *   },
 *   base_table = "statistics_snapshot",
 *   translatable = FALSE,
 *   admin_permission = "administer statistics snapshot entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/admin/statistics/snapshots/{statistics_snapshot}",
 *     "collection" = "/admin/statistics/snapshots",
 *   },
 *   fieldable = TRUE,
 *   field_ui_base_route = "statistics_snapshots.settings_form",
 * )
 */
class StatisticsSnapshot extends ContentEntityBase implements StatisticsSnapshotInterface {

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['start'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Start of snapshot period'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['end'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('End of snapshot period'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['processing_end'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Time processing/calculations ended'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Add a plugin to queue to run the calculation.
   *
   * @param string $plugin_id
   *   Plugin ID.
   */
  public function addPluginToQueue(string $plugin_id) {
    \Drupal::logger('statistics_snapshots')->debug('Adding plugin to queue @plugin', ['@plugin' => $plugin_id]);
    $queue = \Drupal::queue('statistics_snapshots_queue');
    $queue->createItem([
      'snapshot_id' => $this->id(),
      'plugin_id' => $plugin_id,
    ]);
  }

  /**
   * Add a queue item to wrap up after all statistics calculations.
   */
  public function addFinishToQueue() {
    $queue = \Drupal::queue('statistics_snapshots_queue');
    $queue->createItem([
      'snapshot_id' => $this->id(),
      'finish' => TRUE,
    ]);
  }

  /**
   * Initiate stats calculation - called by queue.
   *
   * @param string $plugin_id
   *   Plugin ID.
   * @param array|null $segment_data
   */
  public function calculateStatsForPlugin(string $plugin_id, $segment_data = NULL) {
    $pluginManager = \Drupal::service('plugin.manager.statistics_snapshots_calculator');
    $statsProvider = $pluginManager->createInstance($plugin_id);

    // Only calculate for atemporal plugins, unless the snapshot includes now
    if ($statsProvider->atemporal) {
      $statsProvider->runCalculation($this, $segment_data);
    } else {
      // We have a grace period of 1 hour because of the CRON
      $now = new DrupalDateTime('now');
      if ($now >= $this->start && $now->sub(new DateInterval('P1H')) <= $this->end) {
        $statsProvider->runCalculation($this, $segment_data);
      }
    }
  }

  /**
   * Add calculations to queues to run.
   */
  public function calculateStats() {
    $atemporal = [];
    $temporal = [];

    $pluginManager = \Drupal::service('plugin.manager.statistics_snapshots_calculator');
    $plugins = $pluginManager->getDefinitions();
    foreach ($plugins as $key => $plugin) {
      $statsProvider = $pluginManager->createInstance($key);
      if ($statsProvider->atemporal) {
        $atemporal[] = $key;
      } else {
        $temporal[] = $key;
      }
    }

    // Do temporal first:
    $merged = array_merge($temporal, $atemporal);
    foreach ($merged as $key) {
      $this->addPluginToQueue($key);
    }
    $this->addFinishToQueue();
  }

  /**
   * {@inheritdoc}
   */
  public function setStatValue(StatisticsSnapshotsCalculatorInterface $plugin, string $fieldName, float $value) {
    $field = static::getFieldHash($plugin, $fieldName);

    if (!$this->hasField($field)) {
      throw new StatisticsGenerationException("Statistics cannot find field " . $fieldName);
    }

    $this->set($field, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function incrementStatValue(StatisticsSnapshotsCalculatorInterface $plugin, string $fieldName, $value) {
    \Drupal::logger('statistics_snapshots')->debug('Before: ' . $fieldName . ' - ' . print_r($this->getStatValue($plugin, $fieldName), TRUE));
    $field = static::getFieldHash($plugin, $fieldName);

    if (!$this->hasField($field)) {
      throw new StatisticsGenerationException("Statistics cannot find field " . $fieldName);
    }

    $original_value = $this->getStatValue($plugin, $fieldName) ?? 0;
    $incrementing_value = intval($value);
    $value = $original_value + $incrementing_value;

    \Drupal::logger('statistics_snapshots')->debug('After: ' . $fieldName . ' - ' . print_r($this->getStatValue($plugin, $fieldName), TRUE));
    \Drupal::logger('statistics_snapshots')->debug($plugin->getPluginId() . ' field ' . $fieldName . ' value ' . $original_value . ' incrementing by ' . $incrementing_value . ' equals ' . $value);

    $this->set($field, $value);
  }

  /**
   * {@inheritdoc}
   */
  public static function getFieldHash(StatisticsSnapshotsCalculatorInterface $plugin, string $fieldName) {
    $input = $plugin->getPluginId() . '-' . $fieldName;
    $hash = substr(strtolower(preg_replace('/[0-9_\/]+/', '', base64_encode(sha1($input)))), 0, 30);
    return $hash;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatValue(StatisticsSnapshotsCalculatorInterface $plugin, string $fieldName) {
    $fieldHash = static::getFieldHash($plugin, $fieldName);
    if ($this->hasField($fieldHash) && $this->get($fieldHash)) {
      return $this->get($fieldHash)->value;
    }
    return NULL;
  }

  /**
   * Convert snapshot's period to a nice date range.
   *
   * @param string $format
   *   Date format ID.
   * @param string $separator
   *   How to separate the two dates.
   *
   * @return string
   *   Formatted date range.
   */
  public function getFormattedDateRange(string $format = 'medium', string $separator = ' - ') {
    $dateFormatter = \Drupal::service('date.formatter');

    $start = $dateFormatter->format($this->start->value, $format, $format);
    $end = $dateFormatter->format($this->end->value, $format, $format);
    return $start . $separator . $end;
  }
}
