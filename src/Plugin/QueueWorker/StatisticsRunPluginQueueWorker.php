<?php

namespace Drupal\statistics_snapshots\Plugin\QueueWorker;

use Drupal\commerce_stock_notifications\Form\AdminConfigForm;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;
use Drupal\statistics_snapshots\Entity\StatisticsSnapshot;
use Drupal\statistics_snapshots\Event\SnapshotCreatedEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sends out notifications when items are in stock.
 *
 * @QueueWorker(
 *   id = "statistics_snapshots_queue",
 *   title = @Translation("Statistics Snapshots Queue"),
 *   cron = {"time" = 1}
 * )
 */
class StatisticsRunPluginQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * CommerceStockNotifyQueue constructor.
   *
   * @param array $configuration
   *   Configuration.
   * @param mixed $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin Definition.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $snapshot = StatisticsSnapshot::load($data['snapshot_id']);

    if (isset($data['finish']) && $data['finish']) {
      $queue = \Drupal::queue('statistics_snapshots_queue');

      // If it's not actually the end, don't run finish yet
      if ($queue->numberOfItems() > 1) {
        $snapshot->addFinishToQueue();
        return;
      }

      $snapshot->set('processing_end', strtotime('now'));
      $snapshot->save();

      $event_dispatcher = \Drupal::service('event_dispatcher');
      $event = new SnapshotCreatedEvent($snapshot);
      $event_dispatcher->dispatch(SnapshotCreatedEvent::EVENT_NAME, $event);
    } elseif (isset($data['plugin_id'])) {
      $snapshot->calculateStatsForPlugin($data['plugin_id'], isset($data['segment_data']) ? $data['segment_data'] : NULL);
    }
  }
}
