<?php

namespace Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculator;

use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\Entity\Role;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "user",
 *   label = "User statistics"
 * )
 */
class UserStatsCalculator extends StatisticsSnapshotsCalculatorBase {

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    $fields = [];

    // Count number of active users in total.
    $fields['users'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total active users'));

    // Count number of users who have logged in in period.
    $fields['logged_in'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Users logged in during period'));

    // Count number of users per role.
    $roles = Role::loadMultiple();
    foreach ($roles as $role) {
      if (in_array($role->id(), $this->excludedRoles())) {
        continue;
      }

      $fields['role' . $role->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Users with role: ' . $role->label()));
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  function calculate($snapshot, $segment_data = NULL) {
    // We collect the statistics values together, then set them at the end.
    $values = [];

    // Count number of active users in total.
    $values['users'] = \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->count()
      ->execute();

    // Count number of users who have logged in in period.
    $values['logged_in'] = \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('access', $snapshot->start->value, '>=')
      ->count()
      ->execute();

    // Count number of users per role.
    $roles = Role::loadMultiple();
    foreach ($roles as $role) {
      if (in_array($role->id(), $this->excludedRoles())) {
        continue;
      }

      $values['role' . $role->id()] = \Drupal::entityQuery('user')
        ->condition('status', 1)
        ->condition('roles', $role->id())
        ->count()
        ->execute();
    }

    // Set values on snapshot entity.
    foreach ($values as $fieldName => $count) {
      $snapshot->setStatValue($this, $fieldName, $count);
    }
    $snapshot->save();
  }

  /**
   * Roles we don't generate stats for.
   *
   * @return array
   *   Role IDs to exclude.
   */
  public function excludedRoles() {
    return ['anonymous', 'authenticated'];
  }
}
