<?php

namespace Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculator;

use Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\node\Entity\NodeType;
use Drupal\user\Entity\Role;

/**
 * Class for SU Statistics provider plugin.
 *
 * @StatisticsSnapshotsCalculator(
 *   id = "node",
 *   label = "Node statistics"
 * )
 */
class NodeStatsCalculator extends StatisticsSnapshotsCalculatorBase {

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    $fields = [];

    // Count number of published nodes in total.
    $fields['nodes'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total published nodes'));

    $fields['nodes_up'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total unpublished nodes'));

    // Count number of nodes per type.
    $types = NodeType::loadMultiple();
    foreach ($types as $type) {
      $fields['nodes_' . $type->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Nodes published of type: ' . $type->label()));

      $fields['nodes_up_' . $type->id()] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Nodes unpublished of type: ' . $type->label()));
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  function calculate($snapshot, $segment_data = NULL) {
    // We collect the statistics values together, then set them at the end.
    $values = [];

    // Count number of published nodes in total.
    $values['nodes'] = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->count()
      ->execute();

    $values['nodes_up'] = \Drupal::entityQuery('node')
      ->condition('status', 0)
      ->count()
      ->execute();

    // Count number of nodes per type.
    $types = NodeType::loadMultiple();
    foreach ($types as $type) {
      $values['nodes_' . $type->id()] = \Drupal::entityQuery('node')
        ->condition('status', 1)
        ->condition('type', $type->id())
        ->count()
        ->execute();

      $values['nodes_up_' . $type->id()] = \Drupal::entityQuery('node')
        ->condition('status', 0)
        ->condition('type', $type->id())
        ->count()
        ->execute();
    }

    // Set values on snapshot entity.
    foreach ($values as $fieldName => $count) {
      $snapshot->setStatValue($this, $fieldName, $count);
    }
    $snapshot->save();
  }
}
