<?php

namespace Drupal\statistics_snapshots\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the SU Statistics provider plugin manager.
 */
class StatisticsSnapshotsCalculatorManager extends DefaultPluginManager {

  /**
   * Constructs a new StatisticsSnapshotsCalculatorManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/StatisticsSnapshotsCalculator', $namespaces, $module_handler, 'Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorInterface', 'Drupal\statistics_snapshots\Annotation\StatisticsSnapshotsCalculator');

    $this->alterInfo('statistics_snapshots_statistics_snapshots_calculator_info');
    $this->setCacheBackend($cache_backend, 'statistics_snapshots_statistics_snapshots_calculator_plugins');
  }
}
