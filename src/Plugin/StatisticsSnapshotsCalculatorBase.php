<?php

namespace Drupal\statistics_snapshots\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\statistics_snapshots\Entity\StatisticsSnapshot;
use Drupal\statistics_snapshots\Entity\StatisticsSnapshotInterface;

/**
 * Base class for SU Statistics provider plugins.
 */
abstract class StatisticsSnapshotsCalculatorBase extends PluginBase implements StatisticsSnapshotsCalculatorInterface {

  const SPLIT_CHUNK_SIZE = 1000;

  /**
   * Can be calculated for any period at any time,
   * using e.g. record or order dates
   *
   * The opposite would be if the number is only accurate at the time of checking
   * e.g. for entities that get deleted
   * e.g. student numbers
   */
  public $atemporal = TRUE;

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    $fields = [];

    // Define Base Field Definitions, e.g.

    // $fields['users'] = BaseFieldDefinition::create('integer')
    // ->setLabel(t('Total active users'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  function calculate(StatisticsSnapshotInterface $snapshot, array $segment_data = NULL) {
    // We collect the statistics values together, then set them at the end.
    $values = [];

    // We add to values by e.g. running queries

    // Set values on snapshot entity.
    foreach ($values as $fieldName => $count) {
      $snapshot->setStatValue($this, $fieldName, $count);
    }
    $snapshot->save();
  }

  /**
   * {@inheritdoc}
   */
  public function runCalculation(StatisticsSnapshotInterface $snapshot, array $segment_data = NULL) {
    \Drupal::logger('statistics_snapshots')->debug('Run calculation @plugin @segment', [
      '@plugin' => $this->getPluginId(),
      '@segment' => $segment_data ? ' with segment data (' . count($segment_data) . ' records)' : '',
    ]);
    $this->ensureFields();
    $this->calculate($snapshot, $segment_data);
  }

  /**
   * {@inheritdoc}
   */
  public function splitIntoQueues(StatisticsSnapshotInterface $snapshot, array $dataset) {
    $queue = \Drupal::queue('statistics_snapshots_queue');
    $segments = array_chunk($dataset, static::SPLIT_CHUNK_SIZE);
    foreach ($segments as $segment) {
      $queue->createItem([
        'snapshot_id' => $snapshot->id(),
        'plugin_id' => $this->getPluginId(),
        'segment_data' => $segment,
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function ensureFields() {
    $fields = $this->getFields();

    foreach ($fields as $fieldName => $fieldToEnsure) {
      $fieldName = StatisticsSnapshot::getFieldHash($this, $fieldName);

      $fieldStorage = FieldStorageConfig::loadByName('statistics_snapshot', $fieldName);
      $field = FieldConfig::loadByName('statistics_snapshot', 'statistics_snapshot', $fieldName);

      if (empty($field)) {
        if (!$fieldStorage) {
          $fieldStorage = FieldStorageConfig::create([
            'entity_type' => 'statistics_snapshot',
            'field_name' => $fieldName,
            'type' => $fieldToEnsure->getType()
          ]);
          $fieldStorage->save();
        }

        $field = FieldConfig::create([
          'field_name' => $fieldName,
          'bundle' => 'statistics_snapshot',
          'field_storage' => $fieldStorage,
          'label' => $fieldToEnsure->getLabel(),
        ]);
        $field->save();

        /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
        $display_repository = \Drupal::service('entity_display.repository');
        $display_repository->getViewDisplay('statistics_snapshot', 'statistics_snapshot')
          ->setComponent($fieldName, [
            'label' => 'inline',
            // 'type' => 'text_default',
          ])
          ->save();
      }
    }
  }
}
