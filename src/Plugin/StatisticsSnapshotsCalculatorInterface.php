<?php

namespace Drupal\statistics_snapshots\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\statistics_snapshots\Entity\StatisticsSnapshotInterface;

/**
 * Defines an interface for SU Statistics provider plugins.
 */
interface StatisticsSnapshotsCalculatorInterface extends PluginInspectionInterface {

  /**
   * Define Base Field Definitions for statistics.
   *
   * e.g.
   *
   * $fields['users'] = BaseFieldDefinition::create('integer')
   *  ->setLabel(t('Total active users'));
   *
   * @return array
   *   Array of base field definitions.
   */
  public function getFields();

  /**
   * Calculate the field values and add them to the snapshot.
   *
   * This is a private function - all calculations should be initiated via runCalculation()
   *
   * @param \Drupal\statistics_snapshots\Entity\StatisticsSnapshotInterface $snapshot
   *   Snapshot to save it to.
   * @param array $segment_data
   *   Data to process if we are doing a split/segmented calculation.
   *   See splitIntoQueues.
   */
  function calculate(StatisticsSnapshotInterface $snapshot, array $segment_data = NULL);

  /**
   * Split the processing of a calculation into multiple queue items.
   *
   * By chunking the dataset. A bit like the Batch API.
   *
   * @param \Drupal\statistics_snapshots\Entity\StatisticsSnapshotInterface $snapshot
   * @param array $dataset
   */
  public function splitIntoQueues(StatisticsSnapshotInterface $snapshot, array $dataset);

  /**
   * Ensure fields exist on snapshot entity to store statistics values.
   */
  public function ensureFields();

  /**
   * Initiate the calculation.
   *
   * Always do this instead of calculate to ensure fields etc.
   *
   * @param \Drupal\statistics_snapshots\Entity\StatisticsSnapshotInterface $snapshot
   *   Snapshot to save it to.
   * @param null $segment_data
   *   Data to process if we are doing a split/segmented calculation.
   *   See splitIntoQueues.
   */
  public function runCalculation(StatisticsSnapshotInterface $snapshot, array $segment_data = NULL);
}
