<?php

namespace Drupal\statistics_snapshots\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a SU Statistics calculator annotation object.
 *
 * These are the plugins that calculate the statistics and set the values on the snapshot entity.
 *
 * @see \Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorManager
 * @see plugin_api
 *
 * @Annotation
 */
class StatisticsSnapshotsCalculator extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;
}
