<?php

namespace Drupal\statistics_snapshots;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Statistics snapshot entity.
 *
 * @see \Drupal\statistics_snapshots\Entity\StatisticsSnapshot.
 */
class StatisticsSnapshotAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\statistics_snapshots\Entity\StatisticsSnapshotInterface $entity */

    switch ($operation) {

      case 'view':

        return AccessResult::allowedIfHasPermission($account, 'view statistics snapshot entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit statistics snapshot entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete statistics snapshot entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add statistics snapshot entities');
  }
}
