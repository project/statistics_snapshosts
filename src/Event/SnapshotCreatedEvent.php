<?php

namespace Drupal\statistics_snapshots\Event;

use Druapl\Component\EventDispatcher\Event;
use Drupal\statistics_snapshots\Entity\StatisticsSnapshotInterface;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\Event as EventDispatcherEvent;

/**
 * Event that is fired when a user logs in.
 */
class SnapshotCreatedEvent extends EventDispatcherEvent {

  const EVENT_NAME = 'statistics_snapshots_snapshot_created';

  /**
   * The user account.
   *
   * @var \Drupal\statistics_snapshots\Entity\SnapshotInterface
   */
  public $snapshot;

  /**
   * Constructs the object.
   *
   * @param \Drupal\user\UserInterface $account
   *   The account of the user logged in.
   */
  public function __construct(StatisticsSnapshotInterface $snapshot) {
    $this->snapshot = $snapshot;
  }

  /**
   * @return \Drupal\statistics_snapshots\Plugin\StatisticsSnapshotsCalculatorInterface
   *   Snapshot entity.
   */
  public function getSnapshot() {
    return $this->snapshot;
  }
}
